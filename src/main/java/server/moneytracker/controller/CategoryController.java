package server.moneytracker.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import server.moneytracker.model.Category;
import server.moneytracker.service.CategoryService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/category")
public class CategoryController {

  @Autowired
  CategoryService categoryService;

  @GetMapping()
  public List<Category> getCategorys() {
    return categoryService.retrieveCatagory();
  }

  @GetMapping("/{id}")
  public ResponseEntity<?> getCategory(@PathVariable UUID id) {
    Optional<Category> Category = categoryService.retrieveCatagory(id);
    if(!Category.isPresent()) {
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok(Category);
  }

  @PostMapping()
  public ResponseEntity<?> postCategory(@Valid @RequestBody Category body) {
    Category Category = categoryService.createCatagory(body);
    return ResponseEntity.status(HttpStatus.CREATED).body(Category);
  }

  @PutMapping("/{id}")
  public ResponseEntity<?> putCategory(@PathVariable UUID id, @Valid @RequestBody Category body) {
    Optional<Category> Category = categoryService.updateCatagory(id, body);
    if(!Category.isPresent()) {
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok().build();
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> deleteCategory(@PathVariable UUID id) {
    if(!categoryService.deleteCatagory(id)) {
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok().build();
  }

}