package server.moneytracker.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import server.moneytracker.model.Account;
import server.moneytracker.service.AccountService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/accounts")
public class AccountController {

  @Autowired
  AccountService accountService;

  @GetMapping()
  public List<Account> getAccounts() {
    return accountService.retrieveAccount();
  }

  @GetMapping("/{id}")
  public ResponseEntity<?> getAccount(@PathVariable UUID id) {
    Optional<Account> Account = accountService.retrieveAccount(id);
    if(!Account.isPresent()) {
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok(Account);
  }

  

  @PostMapping()
  public ResponseEntity<?> postAccount(@Valid @RequestBody Account body) {
    Account Account = accountService.createAccount(body);
    return ResponseEntity.status(HttpStatus.CREATED).body(Account);
  }

  @PutMapping("/{id}")
  public ResponseEntity<?> putAccount(@PathVariable UUID id, @Valid @RequestBody Account body) {
    Optional<Account> Account = accountService.updateAccount(id, body);
    if(!Account.isPresent()) {
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok().build();
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> deleteAccount(@PathVariable UUID id) {
    if(!accountService.deleteAccount(id)) {
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok().build();
  }
}