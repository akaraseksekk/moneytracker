package server.moneytracker.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import server.moneytracker.model.RecordItem;
import server.moneytracker.service.RecordItemService;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/recordItems")
public class RecordItemController {

  @Autowired
  RecordItemService recordItemService;


  @GetMapping()
  public List<RecordItem> getRecordItems() {
    System.out.println("xxxxxxx "+recordItemService.retrieveRecordItem());
    return recordItemService.retrieveRecordItem();
  }

  @GetMapping("/{id}")
  public ResponseEntity<?> getRecordItem(@PathVariable UUID id) {
    Optional<RecordItem> RecordItem = recordItemService.retrieveRecordItem(id);
    if(!RecordItem.isPresent()) {
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok(RecordItem);
  }


  @PostMapping()
  public ResponseEntity<?> postRecordItem(@Valid @RequestBody RecordItem body) {
    RecordItem RecordItem = recordItemService.createRecordItem(body);
    return ResponseEntity.status(HttpStatus.CREATED).body(RecordItem);
  }

}