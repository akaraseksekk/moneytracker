package server.moneytracker.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import server.moneytracker.model.AppUser;
import server.moneytracker.service.AppUserService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/appUsers")
public class AppUserController {

  @Autowired
  AppUserService appUserService;

  @GetMapping()
  public List<AppUser> getAppUsers() {
    return appUserService.retrieveAppUser();
  }

  @GetMapping("/{id}")
  public ResponseEntity<?> getAppUser(@PathVariable UUID id) {
    Optional<AppUser> appUser = appUserService.retrieveAppUser(id);
    if(!appUser.isPresent()) {
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok(appUser);
  }

  @GetMapping(params = "name")
  public List<AppUser> getAppUser(@RequestParam(value = "name") String name) {
    return appUserService.retrieveAppUser(name);
  }

  @PostMapping()
  public ResponseEntity<?> postAppUser(@Valid @RequestBody AppUser body) {
    AppUser appUser = appUserService.createAppUser(body);
    return ResponseEntity.status(HttpStatus.CREATED).body(appUser);
  }

  @PutMapping("/{id}")
  public ResponseEntity<?> putAppUser(@PathVariable UUID id, @Valid @RequestBody AppUser body) {
    Optional<AppUser> appUser = appUserService.updateAppUser(id, body);
    if(!appUser.isPresent()) {
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok().build();
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> deleteAppUser(@PathVariable UUID id) {
    if(!appUserService.deleteAppUser(id)) {
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok().build();
  }

}