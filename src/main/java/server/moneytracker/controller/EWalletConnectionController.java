package server.moneytracker.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import server.moneytracker.model.*;
import server.moneytracker.service.RecordItemService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/eWallet")
public class EWalletConnectionController {

    @Autowired
    RecordItemService recordItemService;

    private final String walletUri = "https://5qoen6uljg.execute-api.us-east-2.amazonaws.com/transaction/transactions";

    @GetMapping()
    public String callWallet() throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject jsonObject = new JSONObject();
        jsonObject .put("username", "test");
        jsonObject .put("idx", 90);

        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<String> entity = new HttpEntity<>(jsonObject.toString() , headers);
        ResponseEntity<String> response = restTemplate.postForEntity(this.walletUri, entity, String.class);
        HttpStatus status = response.getStatusCode();
        String result = response.getBody();

        int max = recordItemService.getMaxId();
        List<EWalletHistory> walletTransactions = mapper.readValue(result, new TypeReference<List<EWalletHistory>>() {
        });


        createRecord(walletTransactions);
        return result;
    }

    private void createRecord(List<EWalletHistory> walletTransactions) throws Exception {
        for (EWalletHistory walletTransaction : walletTransactions) {
            RecordItem recordItem = null;
            Date today = new Date();
            if (walletTransaction.getTransactionType().equals("EXPENSE")) {
                recordItem = new Expense();
                ((Expense) recordItem).setExpenseTo("TestShop");
            } else {
                recordItem = new Income();
                ((Income) recordItem).setSourceIncome("Salary");
            }

            Date transactionDate = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(walletTransaction.getTransactionDate());

            recordItem.setRecordType(walletTransaction.getTransactionType());
            recordItem.setCategory(walletTransaction.getCat());
            recordItem.setAmount(walletTransaction.getValue());
            recordItem.setUserId(UUID.fromString("82af5b38-6350-11e9-a923-1681be663d3e"));
            recordItem.setUserName("test");
            recordItem.setWalletId("6424");
            recordItem.setWalletTransactionId(walletTransaction.getId());
            recordItem.setWalletTransactionIdx(walletTransaction.getIdx());
            recordItem.setWalletProvider("test");
            recordItem.setTransactionDate(transactionDate);
            recordItem.setCreatedDate(today);
            recordItem.setLastUpdatedDate(today);
            recordItemService.createRecordItem(recordItem);
        }
    }

}
