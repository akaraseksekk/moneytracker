package server.moneytracker.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class EWalletHistory {

    @JsonProperty
    private UUID id;

    @JsonProperty
    private String username;

    @JsonProperty
    private int idx;

    @JsonProperty
    private String transactionType;

    @JsonProperty
    private String cat;

    @JsonProperty
    private Double value;

    @JsonProperty
    private String transactionDate;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }
}
