package server.moneytracker.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.UUID;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class RecordItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @NotNull
    private String recordType;
//
//    @NotNull
//    private Long categoryId;

    @NotNull
    private String category;

    @NotNull
    private Double amount;

    private String remark;

    @NotNull
    private UUID userId;

    @NotNull
    private String userName;

    @NotNull
    private String walletId;

    @NotNull
    private UUID walletTransactionId;

    @NotNull
    private int walletTransactionIdx;

    @NotNull
    private String walletProvider;

    @NotNull
    private Date transactionDate;

    @NotNull
    private Date createdDate;

    private Date lastUpdatedDate;

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

//    public Long getCategoryId() {
//        return categoryId;
//    }
//
//    public void setCategoryId(Long categoryId) {
//        this.categoryId = categoryId;
//    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public String getUserName() { return userName; }

    public void setUserName(String userName) { this.userName = userName; }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public UUID getWalletTransactionId() {
        return walletTransactionId;
    }

    public void setWalletTransactionId(UUID walletTransactionId) {
        this.walletTransactionId = walletTransactionId;
    }

    public int getWalletTransactionIdx() { return walletTransactionIdx; }

    public void setWalletTransactionIdx(int walletTransactionIdx) { this.walletTransactionIdx = walletTransactionIdx; }

    public String getWalletProvider() {
        return walletProvider;
    }

    public void setWalletProvider(String walletProvider) {
        this.walletProvider = walletProvider;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }


}
