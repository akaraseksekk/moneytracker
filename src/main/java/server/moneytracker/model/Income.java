package server.moneytracker.model;

import lombok.Data;

import javax.persistence.Entity;

@Data
@Entity
public class Income extends RecordItem{

    private String sourceIncome;

    private Double limitedValue;

    public String getSourceIncome() {
        return sourceIncome;
    }

    public void setSourceIncome(String sourceIncome) {
        this.sourceIncome = sourceIncome;
    }

    public Double getLimitedValue() {
        return limitedValue;
    }

    public void setLimitedValue(Double limitedValue) {
        this.limitedValue = limitedValue;
    }
}
