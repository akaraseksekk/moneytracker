package server.moneytracker.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

@Data
@Entity
public class Account {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;

  @NotNull
  @Size(min = 2, max = 50, message = "Please provide first size between 2 - 100")
  private String username;

  @NotNull
  private String accountType;

  @NotNull
  private String sessionKey;

  @NotNull
  private UUID appUserId;

  public void setId(UUID id) {
    this.id = id;
  }
}
