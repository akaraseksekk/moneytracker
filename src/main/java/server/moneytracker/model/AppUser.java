package server.moneytracker.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

@Data
@Entity
public class AppUser {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;

  @NotNull
  @Size(min = 2, max = 50, message = "Please provide first size between 2 - 100")
  private String firstName;

  @NotNull
  @Size(min = 2, max = 100, message = "Please provide lastName size between 2 - 100")
  private String lastName;

  @NotNull
  private Boolean active = Boolean.TRUE;

  @Email(message = "Please provide valid email address")
  private String email;

}
