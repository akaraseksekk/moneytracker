package server.moneytracker.model;

import lombok.Data;

import javax.persistence.Entity;

@Data
@Entity
public class Expense extends RecordItem {
    private String expenseTo;

    public String getExpenseTo() {
        return expenseTo;
    }

    public void setExpenseTo(String expenseTo) {
        this.expenseTo = expenseTo;
    }
}
