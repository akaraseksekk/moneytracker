package server.moneytracker.db;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import server.moneytracker.model.RecordItem;

import java.util.UUID;

public interface RecordItemRepo extends CrudRepository<RecordItem, UUID> {

    @Query("SELECT coalesce(max(ri.walletTransactionIdx), 0) FROM RecordItem ri")
    int getMaxId();
}
