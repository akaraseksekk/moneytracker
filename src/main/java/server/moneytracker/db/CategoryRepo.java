package server.moneytracker.db;

import org.springframework.data.repository.CrudRepository;
import server.moneytracker.model.Category;

import java.util.UUID;

public interface CategoryRepo extends CrudRepository<Category, UUID> {

}
