package server.moneytracker.db;

import org.springframework.data.repository.CrudRepository;
import server.moneytracker.model.AppUser;

import java.util.List;
import java.util.UUID;

public interface AppUserRepo extends CrudRepository<AppUser, UUID> {
  List<AppUser> findByFirstName(String firstName);

}
