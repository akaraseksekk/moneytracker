package server.moneytracker.db;

import org.springframework.data.repository.CrudRepository;
import server.moneytracker.model.Account;

import java.util.UUID;

public interface AccountRepo extends CrudRepository<Account, UUID> {

}
