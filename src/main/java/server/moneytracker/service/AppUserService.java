package server.moneytracker.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import server.moneytracker.db.AppUserRepo;
import server.moneytracker.model.AppUser;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class AppUserService {
    private AppUserRepo appUserRepository;

    @Autowired
    public AppUserService(AppUserRepo repository) {
        this.appUserRepository = repository;
    }

    public List<AppUser> retrieveAppUser() {
        return (List<AppUser>) appUserRepository.findAll();
    }

    public Optional<AppUser> retrieveAppUser(UUID id) {
        return appUserRepository.findById(id);
    }

    public List<AppUser> retrieveAppUser(String name) {
        return appUserRepository.findByFirstName(name);
    }

    public AppUser createAppUser(AppUser appUser) {
        //appUser.setId(null);
        return appUserRepository.save(appUser);
    }

    public Optional<AppUser> updateAppUser(UUID id, AppUser appUser) {
        Optional<AppUser> appUserOptional = appUserRepository.findById(id);
        if (!appUserOptional.isPresent()) {
            return appUserOptional;
        }
        //appUser.setId(id);
        return Optional.of(appUserRepository.save(appUser));
    }

    public boolean deleteAppUser(UUID id) {
        try {
            appUserRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }
}
