package server.moneytracker.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import server.moneytracker.db.AccountRepo;
import server.moneytracker.model.Account;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class AccountService {
  private AccountRepo accountRepository;

  @Autowired
  public AccountService(AccountRepo repository) {
    this.accountRepository = repository;
  }

  public List<Account> retrieveAccount() {
    return (List<Account>) accountRepository.findAll();
  }

  public Optional<Account> retrieveAccount(UUID id) {
    return accountRepository.findById(id);
  }


  public Account createAccount(Account account) {
    //account.setId(null);
    return accountRepository.save(account);
  }

  public Optional<Account> updateAccount(UUID id,
                                         Account account) {
    Optional<Account> accountOptional = accountRepository.findById(id);
    if (!accountOptional.isPresent()) {
      return accountOptional;
    }
    //account.setId(id);
    return Optional.of(accountRepository.save(account));
  }

  public boolean deleteAccount(UUID id) {
    try {
      accountRepository.deleteById(id);
      return true;
    } catch (EmptyResultDataAccessException e) {
      return false;
    }
  }
}
