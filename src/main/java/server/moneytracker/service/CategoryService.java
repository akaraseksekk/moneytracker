package server.moneytracker.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import server.moneytracker.db.CategoryRepo;
import server.moneytracker.model.Category;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CategoryService {
  private CategoryRepo categoryRepository;

  @Autowired
  public CategoryService(CategoryRepo repository) {
    this.categoryRepository = repository;
  }

  public List<Category> retrieveCatagory() {
    return (List<Category>) categoryRepository.findAll();
  }

  public Optional<Category> retrieveCatagory(UUID id) {
    return categoryRepository.findById(id);
  }


  public Category createCatagory(Category catagory) {
    //catagory.setId(null);
    return categoryRepository.save(catagory);
  }

  public Optional<Category> updateCatagory(UUID id,
                                           Category catagory) {
    Optional<Category> catagoryOptional = categoryRepository.findById(id);
    if (!catagoryOptional.isPresent()) {
      return catagoryOptional;
    }
    //catagory.setId(id);
    return Optional.of(categoryRepository.save(catagory));
  }

  public boolean deleteCatagory(UUID id) {
    try {
      categoryRepository.deleteById(id);
      return true;
    } catch (EmptyResultDataAccessException e) {
      return false;
    }
  }
}
