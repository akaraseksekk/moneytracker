package server.moneytracker.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.moneytracker.db.RecordItemRepo;
import server.moneytracker.model.RecordItem;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class RecordItemService {
    private RecordItemRepo recordItemRepository;

    @Autowired
    public RecordItemService(RecordItemRepo repository) {
        this.recordItemRepository = repository;
    }

    public List<RecordItem> retrieveRecordItem() {
        return (List<RecordItem>) recordItemRepository.findAll();
    }

    public Optional<RecordItem> retrieveRecordItem(UUID id) {
        return recordItemRepository.findById(id);
    }

    public RecordItem createRecordItem(RecordItem recordItem) {
        return recordItemRepository.save(recordItem);
    }

    public int getMaxId() { return recordItemRepository.getMaxId(); }
}
